# assignment2
**Description**<br>
This project uses statistical data about the crimes comitted in Switzerland between 2015-2020 for 25 cities.<br>
To use this project run the main class **FT_Pini_Simonek_HistoryMain.java**<br>
This will:
- Configure a database
- Populate the database from data in a csv file named "Gewaltstraftaten auf 1000 Einwohner in ausgewählten Städten.csv"
- Print the table to console
- Display some statistics in the console

Graphs for the statistics can be displayed by running the class **FT_Pini_Simonek_Graph.java**.<br>
JUnit tests must be run seperately in the **JUnitTests.java** class.

Most of the classes have static methods, which can be run directly on the class without creating instances. Only the **FT_Pini_Simonek_HistoryOfCrimes.java** class has a constructor.
<br>
<br>**FT_Pini_Simonek_HistoryMain.java**
- Read csv file and create a ArrayList of HistoryOfCrimes items.
- Database Setup: Create or connect DB, create table.
- Insert tuples from items in ArrayList.
- prints out the table to console.
- Prints out the report header to console.
- Prints out JAVA stats and SQL stats for the year 2020 to console: average, min, max, aboveAverage.


**FT_Pini_Simonek_Graph.java**
- This class shows a graph which displays the number of crimes per year per 1000 inhabitants for 3 selected cities (Basel, Zürich, Bern) for the years 2018, 2019, 2020.


**JUnitTests.java**
- Test that the number of lines read is the same as the number of objects produced.
- Test if the count of rows is the same as read from the file.
- Test if SQL stats are equal to Java stats.
If everything is alright, no error messages are shown. 
