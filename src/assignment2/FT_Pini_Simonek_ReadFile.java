package assignment2;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/* **********************************************************************
 * FT_Pini_Simonek_ReadFile.java
 * Author: Kevin Pini, Andrea Simonek		Date: 08.06.2021
 * HistoryOfCrimes class for a single crime object (row).
 * 
 * Methods: 
 * - readFromFile()
 * - printTable()
 * - parseUglyExcelData()
 * - Getters
 ********************************************************************** */

public class FT_Pini_Simonek_ReadFile {
	static private String city;
	static private double year2015, year2016, year2017, year2018, year2019, year2020;
	static private ArrayList<FT_Pini_Simonek_HistoryOfCrimes> historyList = new ArrayList<FT_Pini_Simonek_HistoryOfCrimes>();
	static private FT_Pini_Simonek_HistoryOfCrimes history;
	static private int maxColumns = 0;
	static private int maxLine = 0;





	// Read file 'AirplaneList.txt' and populate model
	public static ArrayList<FT_Pini_Simonek_HistoryOfCrimes> readFromFile() {
		String str;
		final Integer[] columns = FT_Pini_Simonek_HistoryOfCrimes.getColumns();
		final int firstRow = FT_Pini_Simonek_HistoryOfCrimes.getFirstrow();
		

		
		try {
	        File myObj = new File("Gewaltstraftaten auf 1000 Einwohner in ausgewählten Städten.csv");
	        Scanner myScan = new Scanner(myObj);
	        myScan.nextLine();
	        while (myScan.hasNextLine()) {
	        	maxLine += 1;
	        	str = myScan.nextLine();
				
				// Check which line -> first data line = 10
				if (maxLine >= firstRow - 1) {
					String[] arr = str.split(",");
					if (maxColumns < arr.length) {
						maxColumns = arr.length;
					}
					if (arr.length > 0) {
						// get variables city, 
						try {
							city = arr[columns[0] - 1].replaceAll("\\d", "").trim();
						} catch (Exception e) {
							System.out.println("Error");
						}
						
						// get years 2015-2020 according column numbers
						year2015 = parseUglyExcelData(arr[columns[1] - 1]);
						year2016 = parseUglyExcelData(arr[columns[2] - 1]);
						year2017 = parseUglyExcelData(arr[columns[3] - 1]);
						year2018 = parseUglyExcelData(arr[columns[4] - 1]);
						year2019 = parseUglyExcelData(arr[columns[5] - 1]);
						year2020 = parseUglyExcelData(arr[columns[6] - 1]);
				
						// create history object, add to collection				
						history = new FT_Pini_Simonek_HistoryOfCrimes(city, year2015, year2016, year2017, year2018, year2019, year2020);	
						historyList.add(history);
					}
					else {
						break;
					}		
				}
	        }
	        myScan.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred loading the 'Gewaltstraftaten auf 1000 Einwohner in ausgewählten Städten.csv' file.");
		} 
		return historyList;
	}
	
	// Print table read from csv file
	public static void printTable() {
		System.out.println("TABLE READ FROM FILE");
		for (int i = 0; i < historyList.size(); i++) {
			System.out.println(historyList.get(i).toString());	
		}
	}
	
	// Parse uglyString
	static private double parseUglyExcelData(String uglyString) {		
		try {
			return Double.parseDouble(uglyString);
		} 
		catch (Exception e) {
			String corrected = uglyString.substring(0, uglyString.length() - 2);
			return Double.parseDouble(corrected);
		}
	}
	
	// Getters
	protected static int getLine() {
		return maxLine;
	}
	
	protected static int getMaxColumns() {
		return maxColumns;
	}

	protected static FT_Pini_Simonek_HistoryOfCrimes getHistory() {
		return history;
	}
	
	protected static ArrayList<FT_Pini_Simonek_HistoryOfCrimes> getHistoryList() {
		return historyList;
	}
}
