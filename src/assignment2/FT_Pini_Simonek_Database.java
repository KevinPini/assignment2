package assignment2;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;  
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.sqlite.SQLiteDataSource;  
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

/* **********************************************************************
 * FT_Pini_Simonek_Database.java
 * Author: Kevin Pini, Andrea Simonek		Date: 06.06.2021
 * 
 * Database class: 
 * 
 * Structure:
 * DATABASE SETUP: 	connectDB(), createTable(), insertTuples()
 * REPORT HEADER: 	filePath(), tableName(), countRowsCSV(), 
 * 					countColumnsCSV(), countColumnsDB(), countRowsDB(),
 * 					fileSize(), reportHeader()
 * 
 * STATISTICS: 		averageCrimesSQL(), averageCrimesJava(), 
 * (SQL & JAVA)		minimumCrimesSQL(), minCrimesJava(), maxCrimesSQL(), 
 * 					maxCrimesJava(), citiesAboveAverageSQL(), 
 * 					citiesAboveAverageJava()
 * 
 * CLOSE DB CONNECTION: connectionClose()
 ********************************************************************** */

public class FT_Pini_Simonek_Database {
	public static SQLiteDataSource ds = null;
	public static Connection conn = null;
	
	
//***********************************************************************************	
// DATABASE SETUP
//***********************************************************************************	
	// Create Database if not existing and connect to DB
	public static boolean connectDB(String dbName) {
        try {
            ds = new SQLiteDataSource();
            ds.setUrl("jdbc:sqlite:" + dbName); //
        } catch ( Exception e ) {
            e.printStackTrace();
            System.exit(0);
            return false;
        }
        try {
        	conn = ds.getConnection();
        } catch ( SQLException e ) {
            e.printStackTrace();
            System.exit( 0 );
            return false;
        }
        System.out.println( "Connected to database\n" );
        return true;
    }

    // Create Table
	public static boolean createTable(String tableName) {
		String queryCreate = "CREATE TABLE IF NOT EXISTS " + tableName + 
		"('city' TEXT, " +
		"'2015' REAL, " +
		"'2016' REAL, " +
		"'2017' REAL, " +
		"'2018' REAL, " +
		"'2019' REAL, " +
		"'2020' REAL, " +
		"UNIQUE('city'))";
		
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(queryCreate);
			System.out.println("Table called " + tableName + " created sucessfully!\n");
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
			return false;
		} 
		return true;
	}

	// Insert Data into Database
	public static boolean insertTuples(ArrayList<FT_Pini_Simonek_HistoryOfCrimes> values) {
	    String queryInsert;
		for (int i = 0; i < values.size(); i++) {
			FT_Pini_Simonek_HistoryOfCrimes item = values.get(i);
			queryInsert = "INSERT OR IGNORE INTO crimes ('city', '2015', '2016', '2017', '2018', '2019', '2020') VALUES (" + item.toSQLString() + ")"; 
				System.out.println("Row " + i + " inserted sucessfully!");
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(queryInsert);
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(0);
				return false;
			} 
		}
		System.out.println("All tuples inserted sucessfully\n");
		return true;
	}

	
//***********************************************************************************	
// REPORT HEADER
//***********************************************************************************	
	
	// Where the file is from (file path)
	public static String filePath(String fileName) { 
		File file = new File(fileName);
		if (file.exists()) {
			return "Absolute file path: " + file.getAbsolutePath(); 
		} else {
			return "The file does not exist.";
		}
	}
	 
	// What the file is about (table name)
	public static String tableName() throws SQLException{ 
		Statement stmt  = conn.createStatement();  
		ResultSet rs = stmt.executeQuery("select * from crimes");	  
		ResultSetMetaData rsmd = rs.getMetaData();
		String tablename = rsmd.getCatalogName(1);
		return "This table is about "+ tablename;
	 }
	
	// Count of rows in the original CSV file
	public static int countRowsCSV(String fileName) throws IOException{
		return FT_Pini_Simonek_ReadFile.getLine();
	}
	
	// Count of columns in the original CSV file
	public static int countColumnsCSV(String filename) {
		return FT_Pini_Simonek_ReadFile.getMaxColumns();
	}
	
	// Count columns we have read
	public static int countColumnsDB() throws SQLException{ 
		 Statement stmt  = conn.createStatement();  
		 ResultSet rs = stmt.executeQuery("select * from crimes");	  
		 ResultSetMetaData rsmd = rs.getMetaData();
		 int column_count = rsmd.getColumnCount();
		 return column_count;
	}
 
	// Count rows we have read
	public static int countRowsDB(){ 
		String sql = "SELECT COUNT(*) FROM crimes"; 
		int result = 0; 
		try {  
			Statement stmt  = conn.createStatement();  
			ResultSet rs    = stmt.executeQuery(sql);  
			while (rs.next()) {  
	        //System.out.println("We have read " + rs.getInt("Count(*)") + " rows from the file");  
	        result += rs.getInt("Count(*)");
			}  
		} catch (SQLException e) {  
			System.out.println(e.getMessage());  
		}
		return result;
	}  

	// File size in bytes
	public static long fileSize(String fileName) {  
		File file = new File(fileName);
	    if (file.exists()) {
	    	return file.length();
	    } else {
	    	return -1;
	    }
	} 
	
	// Create Report Header
	public static String reportHeader(String fileName) throws SQLException, IOException {
		String reportHeader = "\nREPORT HEADER:\n";
		reportHeader +=	filePath(fileName) + "\n";
		reportHeader += tableName() + "\n";
		reportHeader += "Total columns of CSV file are: " + countColumnsCSV(fileName) + "\n";
		reportHeader += "Total rows of CSV file are: " + countRowsCSV(fileName) + "\n";
		reportHeader += "Total columns in the database are: " + countColumnsDB() + "\n";
		reportHeader += "Total rows in the database are: " + countRowsDB() + "\n";
		reportHeader += "Filesize is: " + FT_Pini_Simonek_Database.fileSize(fileName) + " bytes\n";
		return reportHeader;
	 }
	   

//***********************************************************************************	
// STATISTICS
//***********************************************************************************	
	
//	 SumSQL --> doesn't make sense for ratio
//	 public static double sumOfCrimes(String year){  
//		 String sql = "select round(SUM(\"" + year + "\"),2) AS sum FROM crimes";  
//		 try {  
//			 Statement stmt  = conn.createStatement();  
//			 ResultSet rs    = stmt.executeQuery(sql);  
//			 return rs.getDouble("sum");  
//		 } catch (SQLException e) {  
//			 return -1.0;  
//		 }
//	 }  

	// averageCrimesSQL	
	public static double averageCrimesSQL(String year){  
		String sql = "select round(avg(\"" + year + "\"),2) AS avr FROM crimes"; 
	    try {  
	        Statement stmt  = conn.createStatement();  
	        ResultSet rs    = stmt.executeQuery(sql);  
	        return rs.getDouble("avr");   
	    } catch (SQLException e) {  
	        return -1.0;  
	    } 
	}  
	
	// averageCrimesJava
	// AverageJava
	public static double averageCrimesJava(String year) {
		String sql = "select \"" + year + "\" AS year  FROM crimes"; 
	    try {  
	        Statement stmt  = conn.createStatement();  
	        ResultSet rs    = stmt.executeQuery(sql);  
	        
	        // loop through the result set  
	        double sum = 0;
	        int count = 0;
            while (rs.next()) {  
            	double value = rs.getDouble("year");
            	sum += value;
            	count += 1;
            }
            
            // Java result
            if (count == 0) {
            	System.out.println("Error, denominator cannot be 0");
            	return -1;
            }
            else {
            	DecimalFormat df = new DecimalFormat("0.00");
            	double result = sum / count;
            	return Double.parseDouble(df.format(result));
            }
	    } catch (SQLException e) {  
	        return -1.0;  
	    } 
	}
	
	// minimumCrimesSQL
	// MinSQL
	public static double minimumCrimesSQL(String year){  
		String sql = "SELECT MIN(\"" + year + "\") AS min FROM crimes"; 
		try { 
			Statement stmt  = conn.createStatement();  
			ResultSet rs    = stmt.executeQuery(sql);  
			return rs.getDouble("min");
		} catch (SQLException e) {  
			return -1.0; 
		} 
	} 
	
	// minCrimesJava
	// MinJAVA
	public static double minCrimesJava(String year) {
		String sql = "select \"" + year + "\" AS year  FROM crimes"; 
		double min = Double.MAX_VALUE;
	    try {  
	        Statement stmt  = conn.createStatement();  
	        ResultSet rs    = stmt.executeQuery(sql);  
	        while (rs.next()) {  
            	double value = rs.getDouble("year"); 
            	if (value < min) {
            		min = value;
            	}
            }
	        return min;
	    } catch (SQLException e) {  
	    	return -1.0;  
	    } 
	}
	
	// maxCrimesSQL
	// MaxSQL
	public static double maxCrimesSQL(String year){  
		String sql = "SELECT MAX(\"" + year + "\") AS max FROM crimes"; 
		try { 
			Statement stmt  = conn.createStatement();  
			ResultSet rs    = stmt.executeQuery(sql);  
			return rs.getDouble("max");
		} catch (SQLException e) {  
			return -1.0; 
		} 
	} 
	
	// maxCrimesJava
	// MaxJAVA
	public static double maxCrimesJava(String year) {
		String sql = "select \"" + year + "\" AS year  FROM crimes"; 
		double max = 0.0;
	    try {  
	        Statement stmt  = conn.createStatement();  
	        ResultSet rs    = stmt.executeQuery(sql);  
	        while (rs.next()) {  
            	double value = rs.getDouble("year"); 
            	if (value > max) {
            		max = value;        
            	}       
	        }
	        return max;
	    } catch (SQLException e) {  
	        return -1.0;  
	    } 
	}
	
	// citiesAboveAverageSQL
	// Cities Above Average SQL
	public static int citiesAboveAverageSQL(String year){  
		String sql = "SELECT count(*) AS total FROM crimes WHERE \"" + year + "\" > (" + averageCrimesSQL(year) + ")";
		try {  
			Statement stmt  = conn.createStatement();  
			ResultSet rs    = stmt.executeQuery(sql); 
			return rs.getInt("total");
		} catch (SQLException e) {  
			return -1;
		} 
	}
	
	// citiesAboveAverageJava
	// Cities Above Average JAVA
	public static int citiesAboveAverageJava(String year) {
		String sql = "select \"" + year + "\" AS year  FROM crimes"; 
	    int count = 0;
	    double avr = averageCrimesJava(year) ; 
	    try {  
	        Statement stmt  = conn.createStatement();  
	        ResultSet rs    = stmt.executeQuery(sql);  
	        while (rs.next()) {  
            	double value = rs.getDouble("year"); 
		        if (value > avr) {			
		        	count += 1;
	            }
            }
	        return count;
	    } catch (SQLException e) {  
	        return -1;  
	    } 
	}
	
	//***********************************************************************************	
	// CLOSE DB CONNECTION
	public static void connectionClose() throws SQLException  {
		conn.close();
		System.out.println("\nDatabase connection is closed");
	}
}



