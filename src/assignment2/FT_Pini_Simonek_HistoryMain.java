package assignment2;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/* **********************************************************************
 * FT_Pini_Simonek_ReadFile.java
 * Author: Kevin Pini, Andrea Simonek		Date: 08.06.2021
 * 
 * Main class Structure:
 * - Database Setup: 	Create or connect DB, create table, insert tuples,
 * 						prints out the table to console.
 * 
 * - Report Header: 	Prints out the report header to console.
 * 
 * - Statistics: 		Prints out JAVA stats and SQL stats to console.
 * 
 * IMPORTANT: SQL tests, Java tests and the graph will not be used here!
 * --> All tests are done in the JUnitTests class.
 * --> The graph is in the FT_Pini_Simonek_Graph class.
 ********************************************************************** */
public class FT_Pini_Simonek_HistoryMain {
	static public ArrayList<FT_Pini_Simonek_HistoryOfCrimes> values;
	private static String fileName = "Gewaltstraftaten auf 1000 Einwohner in ausgewählten Städten.csv";

	public static void main(String[] args) throws SQLException, IOException {
		
		//Database Setup
		values = FT_Pini_Simonek_ReadFile.readFromFile();
		if (!FT_Pini_Simonek_Database.connectDB("HistoryOfCrimes.db")) {return;}
		if (!FT_Pini_Simonek_Database.createTable("crimes")) {return;}
		if (!FT_Pini_Simonek_Database.insertTuples(values)) {return;}
		FT_Pini_Simonek_ReadFile.printTable();
		
		//report header
		System.out.println(FT_Pini_Simonek_Database.reportHeader(fileName));

		//statistics
		System.out.println("STATISTICS SQL");
		System.out.println("SQL: Average crime rate in 2020 per 1000 inhabitants is: " + FT_Pini_Simonek_Database.averageCrimesSQL("2020"));
		System.out.println("JAVA: Result of average crime rate of 2020 is: " + FT_Pini_Simonek_Database.averageCrimesJava("2020"));
		System.out.println("SQL: The city with the lowest crime rate had " + FT_Pini_Simonek_Database.minimumCrimesSQL("2020") + " crimes per 1000 inhabitants in 2020");
		System.out.println("JAVA: Result of min crimes in 2020 is " + FT_Pini_Simonek_Database.minCrimesJava("2020"));
		System.out.println("SQL: The city with the highest crime rate had " + FT_Pini_Simonek_Database.maxCrimesSQL("2020") + " crimes per 1000 inhabitants in 2020");
		System.out.println("JAVA: The Result of max crimes in 2020 is " + FT_Pini_Simonek_Database.maxCrimesJava("2020"));
		System.out.println("SQL: There were " + FT_Pini_Simonek_Database.citiesAboveAverageSQL("2020") + " cities in 2020 where the crime rate per 1000 inhabitants was above average");
		System.out.println("JAVA: There were " + FT_Pini_Simonek_Database.citiesAboveAverageJava("2020") + " cities in 2020 where the crime rate per 1000 inhabitants was above average");
		
		//close DB connection
		FT_Pini_Simonek_Database.connectionClose();
		}
}
