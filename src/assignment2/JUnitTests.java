package assignment2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import org.junit.Assert;

/* **********************************************************************
 * JUnitTests.java
 * Author: Kevin Pini, Andrea Simonek		Date: 08.06.2021
 * 
 * JUnit Tests:
 * - Test that the number of lines read is the same as the number of objects produced
 * - Test if the count of rows is the same as read from the file
 * - Test if SQL stats are equal to Java stats
 ********************************************************************** */

class JUnitTests {
	static public ArrayList<FT_Pini_Simonek_HistoryOfCrimes> values;

	
	@BeforeAll
	public static void testSetup() {
		values = FT_Pini_Simonek_ReadFile.readFromFile();
		if (!FT_Pini_Simonek_Database.connectDB("HistoryOfCrimes.db")) {return;}
		if (!FT_Pini_Simonek_Database.createTable("crimes")) {return;}
		if (!FT_Pini_Simonek_Database.insertTuples(values)) {return;}

	}

	// Test that the number of lines read is the same as the number of objects produced
	@Test
	public void testReadFile() {
		assertTrue(FT_Pini_Simonek_HistoryOfCrimes.counter == FT_Pini_Simonek_ReadFile.getHistoryList().size(), "Number of lines read is not the same as the number of objects produced");
	}
	
	// Test if the count of rows is the same as read from the file
	@Test 
	public void compareSQLToJAVA() {
		assertTrue(FT_Pini_Simonek_Database.countRowsDB() == FT_Pini_Simonek_ReadFile.getHistoryList().size(), "Number of objects read from file is not the same as the number of objects produced in database");	
	}
	
	// Test if SQL stats are equal to Java stats
	@Test
	public void checkStats() {
		// Average
		assertTrue(FT_Pini_Simonek_Database.averageCrimesSQL("2020") == FT_Pini_Simonek_Database.averageCrimesJava("2020"), "SQL average is not equal to Java average for 2020");
		
		// Min
		assertTrue(FT_Pini_Simonek_Database.minimumCrimesSQL("2020") == FT_Pini_Simonek_Database.minCrimesJava("2020"), "SQL min is not equal to Java min for 2020");
	
		// Max
		assertTrue(FT_Pini_Simonek_Database.maxCrimesSQL("2020") == FT_Pini_Simonek_Database.maxCrimesJava("2020"), "SQL max is not equal to Java max for 2020");
	
		// citiesAboveAverage
		assertTrue(FT_Pini_Simonek_Database.citiesAboveAverageSQL("2020") == FT_Pini_Simonek_Database.citiesAboveAverageJava("2020"), "SQL citiesAboveAverage is not equal to Java citiesAboveAverage for 2020");
	}
}
	
	