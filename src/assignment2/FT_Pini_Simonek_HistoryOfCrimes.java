package assignment2;

/* **********************************************************************
 * FT_Pini_Simonek_HistoryOfCrimes.java
 * Author: Kevin Pini, Andrea Simonek		Date: 08.06.2021
 * HistoryOfCrimes class for a single crime object (row).
 * 
 * Methods:
 * - constructor in which the columns to be read are defined
 * - blank Constructor
 * - toString method for SQL query
 * - getters and setters
 ********************************************************************** */

public class FT_Pini_Simonek_HistoryOfCrimes {
	private String city;
	static int counter;
	private double year2015, year2016, year2017, year2018, year2019, year2020;
	private final static Integer[] columns = {2, 10, 11, 12, 13, 14, 15};
	private final static int firstRow = 10;
	
	//----------------------------------------------------------------------------------
	// Constructor with initialization
	public FT_Pini_Simonek_HistoryOfCrimes(String city, double year2015, double year2016, double year2017, double year2018, double year2019, double year2020 ) {
		this.city = city;
		this.year2015 = year2015;
		this.year2016 = year2016;
		this.year2017 = year2017;
		this.year2018 = year2018;
		this.year2019 = year2019;
		this.year2020 = year2020;
		FT_Pini_Simonek_HistoryOfCrimes.counter += 1;
	}
	
	// Blank Constructor
	public FT_Pini_Simonek_HistoryOfCrimes() {
		FT_Pini_Simonek_HistoryOfCrimes.counter += 1;
	}
	
	// ToString method
	public String toString() {
		return city + ", " + year2015 + ", " + year2016 + ", " + year2017 + ", " + year2018 + ", " + year2019 + ", " + year2020;
	}
	
	// ToString method for SQL query
	public String toSQLString() {
		return "'" + city + "', '" + year2015 + "', '" + year2016 + "', '" + year2017 + "', '" + year2018 + "', '" + year2019 + "', '" + year2020 + "'";
	}
	
	// Getters and Setters
	protected String getCity() {
		return city;
	}
	protected void setCity(String city) {
		this.city = city;
	}
	protected double getYear2015() {
		return year2015;
	}
	protected void setYear2015(double year2015) {
		this.year2015 = year2015;
	}
	protected double getYear2016() {
		return year2016;
	}
	protected void setYear2016(double year2016) {
		this.year2016 = year2016;
	}
	protected double getYear2017() {
		return year2017;
	}
	protected void setYear2017(double year2017) {
		this.year2017 = year2017;
	}
	protected double getYear2018() {
		return year2018;
	}
	protected void setYear2018(double year2018) {
		this.year2018 = year2018;
	}
	protected double getYear2019() {
		return year2019;
	}
	protected void setYear2019(double year2019) {
		this.year2019 = year2019;
	}
	protected double getYear2020() {
		return year2020;
	}
	protected void setYear2020(double year2020) {
		this.year2020 = year2020;
	}
	protected static Integer[] getColumns() {
		return columns;
	}
	protected static int getFirstrow() {
		return firstRow;
	}
}
