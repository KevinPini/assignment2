package assignment2;
import java.sql.Connection;
import java.io.IOException;
import java.sql.SQLException;
import org.sqlite.SQLiteDataSource;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import java.sql.ResultSet;
import java.sql.Statement;

/* **********************************************************************
 * FT_Pini_Simonek_Graph.java
 * Author: Kevin Pini, Andrea Simonek		Date: 08.06.2021
 * 
 * Graph class --> reads data from the Database HistoryOfCrimes.db
 * Displays a Gui which shows a bar chart including:
 * - xAxis = Cities
 * - yAxis = Number of Crimes per 1000 inhabitants
 *  
 * IMPORTANT: This class is not used in the FT_Pini_Simonek_HistoryMain class
 * --> In order to show the graph this class must be runned.
 * Methods: connectDB(), crimesYearCity(), start(), connectionClose()
 ********************************************************************** */

public class FT_Pini_Simonek_Graph extends Application {
	public static SQLiteDataSource ds = null;
	public static Connection conn = null;
	
	// Connect to DB
	public static boolean connectDB(String dbName) {
		try {
			ds = new SQLiteDataSource();
            ds.setUrl("jdbc:sqlite:" + dbName); //
	        } catch ( Exception e ) {
	            e.printStackTrace();
	            System.exit(0);
	            return false;
	        }
	        try {
	        	conn = ds.getConnection();
	        } catch ( SQLException e ) {
	            e.printStackTrace();
	            System.exit( 0 );
	            return false;
	        }
	        return true;
	    }

	// Setup SQLite query for bar chart
	public static double crimesYearCity(String city, String year){  
		String crimesYearCity = "SELECT \"" + year + "\" AS crimesYearCity FROM crimes WHERE city = \"" + city + "\"";
		try {  
			Statement stmt  = conn.createStatement();  
			ResultSet rs    = stmt.executeQuery(crimesYearCity);  
			return rs.getDouble("crimesYearCity");
		} catch (SQLException e) {  
			return -1.0;
		}
	}

	// Start GUI
	public void start(Stage stage) {
		final CategoryAxis xAxis = new CategoryAxis();
	    final NumberAxis yAxis = new NumberAxis();
	    final BarChart<String,Number> bc = new BarChart<String,Number>(xAxis,yAxis);
	    
	    // Setup Chart
	    stage.setTitle("Bar Chart - crime rate of selected cities in Switzerland");
	    bc.setTitle("Crime rate Basel, Zürich, Bern per 1000 inhabitants");
	    xAxis.setLabel("City");       
	    yAxis.setLabel("Crimes per year");

	    // Series 1  
	    XYChart.Series series1 = new XYChart.Series();
        series1.setName("2018");       
        series1.getData().add(new XYChart.Data("Basel", crimesYearCity("Basel","2018")));
        series1.getData().add(new XYChart.Data("Zürich", crimesYearCity("Zürich","2018")));
        series1.getData().add(new XYChart.Data("Bern", crimesYearCity("Bern","2018")));
         
        // Series 2
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("2019");
        series2.getData().add(new XYChart.Data("Basel", crimesYearCity("Basel","2019")));
        series2.getData().add(new XYChart.Data("Zürich", crimesYearCity("Zürich","2019")));
        series2.getData().add(new XYChart.Data("Bern", crimesYearCity("Bern","2019")));

        // Series 3
        XYChart.Series series3 = new XYChart.Series();
        series3.setName("2020");
        series3.getData().add(new XYChart.Data("Basel", crimesYearCity("Basel","2019")));
        series3.getData().add(new XYChart.Data("Zürich", crimesYearCity("Zürich","2019")));
        series3.getData().add(new XYChart.Data("Bern", crimesYearCity("Bern","2019")));
 
        // Initialize Stage
        Scene scene  = new Scene(bc,800,600);
        bc.getData().addAll(series1, series2, series3);
        stage.setScene(scene);
        stage.show();
    }
	    
  	//close DB connection
	public static void connectionClose() throws SQLException  {		
		conn.close();
	}
	
	//main method
    public static void main(String[] args) throws SQLException, IOException  {
    	if (!FT_Pini_Simonek_Graph.connectDB("HistoryOfCrimes.db")) {return;}
		launch(args);
        FT_Pini_Simonek_Graph.connectionClose();
    }
}
